﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.Experimental.GraphView;

public abstract class SampleNode : Node
{
    public Port InputPort;
    public Port OutputPort;
    //public int IntValue;
    public string StringValue;
    
    // public SampleNode()
    // {
    //     title = "Sample";
    //
    //     var inputPort = Port.Create<Edge>(Orientation.Horizontal, Direction.Input, Port.Capacity.Single, typeof(Port));
    //     inputContainer.Add(inputPort);
    //
    //     var outputPort = Port.Create<Edge>(Orientation.Horizontal, Direction.Output, Port.Capacity.Single, typeof(Port));
    //     outputContainer.Add(outputPort);
    // }

    public void ProcessNode()
    {
        InputPort = Port.Create<Edge>(Orientation.Horizontal, Direction.Input, Port.Capacity.Single, typeof(Port));
        InputPort.portName = "In";
        inputContainer.Add(InputPort);

        OutputPort = Port.Create<Edge>(Orientation.Horizontal, Direction.Output, Port.Capacity.Single, typeof(Port));
        OutputPort.portName = "Out";
        outputContainer.Add(OutputPort);
    }
    
    public abstract void Execute();

    public int IntValue
    {
        get
        {
            return IntValue;
        }
        set
        {
            IntValue=value;
        }
    }

}

