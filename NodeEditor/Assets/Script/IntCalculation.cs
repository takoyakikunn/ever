﻿using System.ComponentModel;
using System.Linq;
using UnityEngine;
using UnityEditor.Experimental.GraphView;
using UnityEditor.UIElements;

public class IntCalculation : SampleNode
{
    private Port outPut;
    private Port inputintA;
    private Port inputintB;

    public IntCalculation() : base()
    {
        ProcessNode();
        title = "IntCalculation A+B";

        inputintA = Port.Create<Edge>(Orientation.Horizontal, Direction.Input, Port.Capacity.Single, typeof(int));
        inputintA.name = "A";
        inputintA.portName = "A";
        inputContainer.Add(inputintA);

        inputintB = Port.Create<Edge>(Orientation.Horizontal, Direction.Input, Port.Capacity.Single, typeof(int));
        inputintA.name = "B";
        inputintB.portName = "B";
        inputContainer.Add(inputintB);

        outPut = Port.Create<Edge>(Orientation.Horizontal, Direction.Output, Port.Capacity.Multi, typeof(int));
        outPut.name = "Result";
        outPut.portName = "ResultPort";
        outputContainer.Add(outPut);
        
    }

    public override void Execute()
    {
        var edge1 = inputintA.connections.FirstOrDefault();
        var node = edge1.output.node as SampleNode;
        var edge2 = inputintB.connections.FirstOrDefault();
        var node2 = edge2.output.node as SampleNode;

        int num = node.IntValue+node2.IntValue;

        IntValue = num;
    }

}
