﻿using System.Linq;
using UnityEngine;
using UnityEditor.Experimental.GraphView;
using UnityEngine.UIElements;


public class IntLog : SampleNode
{
    private Port inputString;

    
    public IntLog() : base()
    {
        ProcessNode();
        title = "IntLog";

        inputString = Port.Create<Edge>(Orientation.Horizontal, Direction.Input, Port.Capacity.Single, typeof(int));
        inputString.name = "intLog_Name";
        inputContainer.Add(inputString);
    }

    public override void Execute()
    {
        var edge = inputString.connections.FirstOrDefault();
        SampleNode node = edge.output.node as SampleNode;
        IntValue = node.IntValue;

        if (node.title == "IntNode")
        {
            IntNode node2 = edge.output.node as IntNode;
            IntValue = node2.GetIntValue();
        }
        
        if (node == null) return;

        Debug.Log(IntValue);
    }
    
    
}
