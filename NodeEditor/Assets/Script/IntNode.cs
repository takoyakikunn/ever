﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.Experimental.GraphView;
using UnityEditor.UIElements;
using UnityEngine.UI;

public class IntNode : SampleNode
{
    private IntegerField textField;
  
    public int GetIntValue()
    {
        return textField.value;
    }

    
    public IntNode()
    {
        title = "IntNode";

        var outputPort = Port.Create<Edge>(Orientation.Horizontal, Direction.Output, Port.Capacity.Multi, typeof(int));
        outputContainer.Add(outputPort);
        textField = new IntegerField();
        mainContainer.Add(textField);
    }

   public override void Execute()
   {
    //   IntValue = int.Parse(textField.text);
   }
   
   
   
   
}