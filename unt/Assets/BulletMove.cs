﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour
{
    int animKind=0;
    [SerializeField]
    float speed = 20;

    void Start()
    {
        StartCoroutine(Move());
        StartCoroutine(DestroyMe());
    }

    IEnumerator Move()
    {
        while (true)
        {
            Vector3 velocity = gameObject.transform.rotation * new Vector3(speed, 0, 0);
            gameObject.transform.position += velocity * Time.deltaTime;

            //Vector3 pos=gameObject.transform.localPosition;
            //gameObject.transform.localPosition= new Vector3(pos.x,);
            yield return null;
        }
    }

    IEnumerator DestroyMe()
    {
        yield return new WaitForSeconds(5);
        Destroy(gameObject);
    }
    private void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject);
    }


}
