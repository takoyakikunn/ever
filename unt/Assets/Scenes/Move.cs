﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Move : MonoBehaviour
{
    [SerializeField]
    GameObject obj;
    [SerializeField]
    float Speed = 1;
    [SerializeField]
    int hp=3;
    [SerializeField]
    float mutekiTime=4;
    [SerializeField]
    Text text;  


    float moveLength = 1;
    bool mutekiFlag=false;
    enum HOUKOU_KIND
    {
        UP = 90,
        RIGHT = 0,
        LEFT = 180,
        DOWN = 270,
    }

    HOUKOU_KIND houkou;

    private void Start()
    {
            text.text = hp.ToString();
        houkou = HOUKOU_KIND.UP;
        StartCoroutine(Shot());
    }

    IEnumerator Shot()
    {
        while (true)
        {
            yield return new WaitForSeconds(Speed);
            Instantiate(obj, gameObject.transform.position, /*new Quaternion(0, 0, (float)houkou*/Quaternion.Euler(0, 0, (float)houkou));
            Debug.Log(houkou);
            Debug.Log((float)houkou);
        }
    }

    public void Up()
    {
        Vector3 pos = gameObject.transform.localPosition;
        if (pos.y >= 4)
            return;
            gameObject.transform.localPosition = new Vector3(pos.x, pos.y + moveLength, pos.z);
        houkou = HOUKOU_KIND.UP;
    }

    public void Left()
    {
        Vector3 pos = gameObject.transform.localPosition;

        if (pos.x <= -6)
            return;

        gameObject.transform.localPosition = new Vector3(pos.x - moveLength, pos.y, pos.z);
        houkou = HOUKOU_KIND.LEFT;
    }

    public void Right()
    {
        Vector3 pos = gameObject.transform.localPosition;

        if (pos.x >= 6)
            return;

        gameObject.transform.localPosition = new Vector3(pos.x + moveLength, pos.y, pos.z);
        houkou = HOUKOU_KIND.RIGHT;
    }

    public void Down()
    {
        Vector3 pos = gameObject.transform.localPosition;
        if (pos.y <= -2)
            return;
        gameObject.transform.localPosition = new Vector3(pos.x, pos.y - moveLength, pos.z);
        houkou = HOUKOU_KIND.DOWN;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (mutekiFlag)
            return;
        hp--;
        text.text = hp.ToString();
        if (hp <= 0)
        {
            Destroy(gameObject);
            return;
        }
        StartCoroutine(Muteki());
    }

    IEnumerator Muteki()
    {
        mutekiFlag = true;
        yield return new WaitForSeconds(mutekiTime);
        mutekiFlag = false;
    }

}
