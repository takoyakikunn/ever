﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossProcess : MonoBehaviour
{
    Animation ani;
    [SerializeField]
    int hp=20;
    [SerializeField]
    Text text;

    void Start()
    {
            text.text = hp.ToString();
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.tag);
        if (other.tag == "Bullet")
        {
            hp--;
            text.text = hp.ToString();

            if (hp <= 0)
            {
                Destroy(gameObject);
            }
        }


    }

}
